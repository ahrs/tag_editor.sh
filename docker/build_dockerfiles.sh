#!/bin/sh

cd .. || exit 1

if [ "$(id -u)" -ne 0 ]
then
  SUDO="sudo"
else
  SUDO=""
fi

for f in ./docker/Dockerfile.*
do
  set -x
  TAG="$(printf "%s" "$f" | rev | sed 's/\..*//g' | rev)"
  $SUDO docker build --no-cache -t local/tag_editor:"$TAG" . -f "$f"
  $SUDO docker tag local/tag_editor:"$TAG" registry.gitlab.com/ahrs/tag_editor.sh:"$TAG"
  $SUDO docker push registry.gitlab.com/ahrs/tag_editor.sh:"$TAG"
  set +x
done
