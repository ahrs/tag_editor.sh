#!/bin/sh

EDITOR_CONFIG=1 # Whether or not to generate an editorconfig file to suggest to editors how to format the yaml files
USE_RANGER=1    # Whether or not to use ranger as a file picker - The default is yes, without this we'll recursively edit all files contained in a folder

[ -z   "$XDG_MUSIC_DIR" ] && XDG_MUSIC_DIR="$HOME/Music"
[ ! -d "$XDG_MUSIC_DIR" ] && XDG_MUSIC_DIR="$HOME"

errexit() {
  [ -n "$1" ] && printf "%s\\n" "$1" > /dev/stderr
  [ -n "$2" ] && exit "$2" || exit 1
}

usage() {
  _XDG_MUSIC_DIR="$XDG_MUSIC_DIR"
  [ "$_XDG_MUSIC_DIR" != "$HOME" ] && _XDG_MUSIC_DIR="$(printf "%s" "$_XDG_MUSIC_DIR" | sed "s#$HOME#~#g")"
  printf "Usage:\\n"
  printf "    %s - Launches ranger at %s\\n" "$0" "$_XDG_MUSIC_DIR"
  printf "    %s [files...] [folders...] - Edit files / folders specified by arguments\\n" "$0"

  return 0
}

no_shyaml_dependency() {
  printf "%s\\n" "shyaml is needed for parsing yaml.\\nYou can install it as follows:\\n    pip install --user shyaml\\n" > /dev/stderr

  return 1
}

editor() {
  if [ -n "$EDITOR" ]
  then
    # `command` is used to handle the edge-case where EDITOR is set to "editor"
    # we don't want to recursively call ourself
    command "$EDITOR" "$@"

    return "$?"
  fi

  if command -v nvim > /dev/null 2> /dev/null
  then
    nvim "$@"
  elif command -v vim > /dev/null 2> /dev/null
  then
    vim "$@"
  elif command -v nano > /dev/null 2> /dev/null
  then
    nano "$@"
  elif command -v micro > /dev/null 2> /dev/null
  then
    micro "$@"
  elif command -v emacs > /dev/null 2> /dev/null
  then
    emacs "$@"
  elif command -v vi > /dev/null 2> /dev/null
  then
    vi "$@"
  fi

  return "$?"
}

check_dependencies() {
  DEPS="
    shyaml `# For parsing yaml`
  "
  HAS_DEPS=1
  printf "%s" "$DEPS" | while read -r dep
  do
    [ -z "$dep" ] && continue
    if ! command -v "$dep" > /dev/null 2> /dev/null
    then
      eval "no_${dep}_dependency"
      HAS_DEPS=0
    fi
    [ "$HAS_DEPS" -eq 1 ] # This is the last command executed so $? will be set to it. We can't test for HAS_DEPS due to it being in a subshell
  done

  # shellcheck disable=SC2181
  [ "$?" -ne 0 ] && exit 1
}

musicbrainz_find_match() {
  ARTIST=""
  QUERY="$1"
  [ -z "$QUERY" ] && return 1
  [ -n "$2" ] && ARTIST=" AND artist:\"$2\""
  curl \
    -G \
    -s \
    -L \
    --header 'Accept: application/json' \
    --data-urlencode "query=\"$QUERY\"$ARTIST" https://www.musicbrainz.org/ws/2/recording
}

musicbrainz_get_coverart() {
  [ -z "$1" ] && return 1

  curl \
    -G \
    -s \
    -L \
    --header 'Accept: application/json' \
    https://coverartarchive.org/release/"$1"
}

# $1 audio file
# $2 tags
# $3 filename
update_tags() {
  if [ -z "$TMPDIR" ]
  then
    [ -d /dev/shm ] && TMPDIR=/dev/shm || TMPDIR=/tmp
  fi

  if [ ! -d "$TMPDIR" ]
  then
    mkdir -p "$TMPDIR" || errexit "Couldn't create TMPDIR: $TMPDIR"
  fi

  keys="$(printf "%s" "$2" | shyaml keys)"
  metadata="$(mktemp)"

  printf "%s" "$keys" | while read -r key
  do
    [ -z "$key" ] && continue
    value="$(printf "%s" "$2" | shyaml get-value "$key")"

    #printf "key: %s\\n" "$key"
    #printf "value: %s\\n" "$value"
    #printf "\\n"

    printf '%s %s="%s" ' "-metadata" "${key}" "${value}" >> "$metadata"
  done

  _metadata="$metadata"
  metadata="$(cat "$metadata")"
  rm -f "$_metadata"
  unset _metadata

  #printf "metadata: %s\\n" "$metadata"

  # There must be a better way to do this than using eval?
  # shellcheck disable=SC2016
  cmd="$(printf "ffmpeg -v quiet -i \"%s\" -codec copy %s \"%s\"" "$1" "$metadata" "$TMPDIR/$(basename "$3")" | sed -e 's/\\/\\\\/g' -e 's/`/\\`/g' -e 's/\$/\\$/g' -e 's/;/\\;/g')"

  eval "$cmd"

  # To Do:
  #     * Move the temporary file here:
  #         - Re-name the original to original.tmp
  #         - Move the tmp file to original
  #         - Delete original.tmp

  return "$?"
}

# $1 - Mimetype of file to compare
is_audio_file() {
 # There are probably other mime types to add to this list
 MIMES="
    application/octet-stream
    audio/mpeg
    audio/ogg
    audio/x-aiff
    audio/x-flac
    audio/x-m4a
    audio/x-wav
  "

  printf "%s" "$MIMES" | while read -r mime
  do
    [ -z "$mime" ] && continue
    [ "$mime" = "$1" ] && break
  done

  return "$?"
}

edit_tags() {
  EXAMPLE_TAGS="$(printf "\\ttitle: Cool Song Title\\n")"
  for arg in "$@"
  do
    _mime="$(file -L --mime-type --brief "$arg")"
    if [ "$_mime" = "inode/directory" ]
    then
      FILES="$(mktemp)"
      if [ "$HAS_RANGER" -eq 1 ] && [ "$USE_RANGER" -eq 1 ]
      then
        ranger --choosefiles="$FILES" "$arg"
      else
        # To Do:
        #   * Perhaps set max-depth here, what happens if someone runs tag_editor.sh on /, etc?
        find -L "$arg" -type f >> "$FILES"
      fi

      while read -r file
      do
        [ -z "$file" ] && continue
        edit_tags "$file"
      done < "$FILES"
      rm -f "$FILES"
    elif is_audio_file "$_mime"
    then
      TAGS="$(ffprobe -v quiet -print_format json -show_format "$arg" | shyaml get-value format.tags '' | sed 's/^/    /g')"
      [ "${#TAGS}" -eq 1 ] && TAGS="$EXAMPLE_TAGS"
      FILE="$(mktemp)"
      mv "$FILE" "${FILE}.yaml"
      FILE="${FILE}.yaml"

      SUGGESTED_METADATA_FILE="$(mktemp)"

      if [ "$HAS_JQ" -eq 1 ]
      then
        TITLE="$(printf "tags:\\n%s" "$TAGS" | shyaml get-value tags.title '')"
        ARTIST="$(printf "tags:\\n%s" "$TAGS" | shyaml get-value tags.artist '')"
        ALBUM_ARTIST="$(printf "tags:\\n%s" "$TAGS" | shyaml get-value tags.album_artist '')"
        [ -z "$ARTIST" ] && [ -n "$ALBUM_ARTIST" ] && ARTIST="$ALBUM_ARTIST"

        JSON="$(musicbrainz_find_match "$TITLE" "$ARTIST")"
        COUNT="$(printf "%s" "$JSON" | jq -r '.count')"
        [ -z "$COUNT" ] && COUNT=0
        if [ "$COUNT" -gt 0 ]
        then
          ARTIST="$(printf "%s" "$JSON" | jq -r '.recordings[0]["artist-credit"][0].artist.name')"
          RELEASE="$(printf "%s" "$JSON" | jq -r '.recordings[0].releases[0]')"
          MBID="$(printf "%s" "$RELEASE" | jq -r '.id')"
          ALBUM="$(printf "%s" "$RELEASE" | jq -r '.title')"
          MEDIA="$(printf "%s" "$RELEASE" | jq -r '.media[0]')"
          TITLE="$(printf "%s" "$MEDIA" | jq -r '.track[0].title')"
          TRACK="$(printf "%s" "$MEDIA" | jq -r '.position')/$(printf "%s" "$MEDIA" | jq -r '.["track-count"]')"
          COVER_ART="$(musicbrainz_get_coverart "$MBID" | jq -r '.images[0].image' 2> /dev/null)"
          [ -n "$COVER_ART" ] && COVER_ART="    album_cover: $COVER_ART"
          printf "suggested_tags:\\n%s\\n%s\\n%s\\n%s\\n%s\\n" \
            "    title: \"$TITLE\"" \
            "    album: \"$ALBUM\"" \
            "    track: \"$TRACK\"" \
            "    artist: \"$ARTIST\"" \
            "$COVER_ART" > "$SUGGESTED_METADATA_FILE"
        fi
      fi

      SUGGESTED_METADATA="$(cat "$SUGGESTED_METADATA_FILE")"
      rm -f "$SUGGESTED_METADATA_FILE"
      unset SUGGESTED_METADATA_FILE

      printf "filename: %s\\ntags:\\n%s\\n%s\\n# vim: set sw=4:\\n" "$(basename "$arg")" "$TAGS" "$SUGGESTED_METADATA" > "$FILE"

      # http://editorconfig.org
      if [ "$EDITOR_CONFIG" -eq 1 ]
      then
        cat <<EOF > "$(dirname "$FILE")/.editorconfig"
# top-most EditorConfig file
root = true

# Unix-style newlines with a newline ending every file
[tmp.*.yaml]
end_of_line = lf
insert_final_newline = true
indent_style = space
indent_size = 4
EOF
      fi

      editor "$FILE" < /dev/tty

      # The sed is important since shyaml won't parse files using tab (\t) characters for indentation
      _TAGS="$(sed -e 's/\t/    /g' "$FILE" | shyaml get-value tags | sed 's/^/    /g')"
      FILENAME="$(sed -e 's/\t/    /g' "$FILE" | shyaml get-value filename "$(basename "$arg")")"

      rm -f "$FILE"

      # Only update the tags if they've changed
      if [ "$TAGS" != "$_TAGS" ] || [ "$(basename "$arg")" != "$FILENAME" ]
      then
        update_tags "$arg" "$_TAGS" "$FILENAME"
      fi
    fi
  done
}

if [ "$1" = "-h" ] || [ "$1" = "--help" ]
then
  usage
  exit "$?"
fi

# shyaml might have been `pip installed` but the PATH not exported
[ -f "$HOME/.local/bin/shyaml" ] && PATH="$PATH:$HOME/.local/bin"; export PATH

check_dependencies
command -v ranger > /dev/null 2> /dev/null && HAS_RANGER=1 || HAS_RANGER=0
command -v jq     > /dev/null 2> /dev/null && HAS_JQ=1     || HAS_JQ=0

if [ -z "$1" ]
then
  if [ "$HAS_RANGER" -eq 1 ] && [ "$USE_RANGER" -eq 1 ]
  then
    FILES="$(mktemp)"
    ranger --choosefiles="$FILES" "$XDG_MUSIC_DIR"
    _FILES="$(cat "$FILES")"
    rm -f "$FILES"
    edit_tags "$_FILES"
    exit "$?"
  else
    usage
    exit "$?"
  fi
else
  edit_tags "$@"

  exit "$?"
fi

exit 0
