# tag_editor.sh

A simple audio tag editor written in POSIX SH.

## Dependencies

* a posix compliant shell
* [ffmpeg](https://ffmpeg.org/) (for editing metadata)
* [ranger](https://github.com/ranger/ranger) (optional dependency for picking files/folders)
* [curl](https://curl.haxx.se/) (optional dependency for querying [musicbrainz](https://musicbrainz.org))
* [jq](https://stedolan.github.io/jq/) (optional dependency for parsing response data from musicbrainz)
* [shyaml](https://github.com/0k/shyaml) for parsing yaml

## Usage

[![asciicast](https://asciinema.org/a/LNTca9z8yan7EFJHB1WkqjIql.png)](https://asciinema.org/a/LNTca9z8yan7EFJHB1WkqjIql)

tag_editor.sh uses your default editor to edit song metadata. If your `EDITOR` is not set it will try to fallback to a default editor. Tags are edited using the yaml format.

For an example of the tag names that can be used see the following: https://wiki.multimedia.cx/index.php/FFmpeg_Metadata

Re-tagged media will be placed in `TMPDIR` (either /dev/shm or /tmp unless you explicitely set this). Once I'm confident that the software functions correctly it will override the original files.

### Ranger Integration

You can easily add a keybinding to ranger. For example:

`map <c-t> open_with tag_editor.sh`

Ensure that `tag_editor.sh` is in your `PATH` and when you press `<Control>+t` ranger will open the selected song in tag_editor.

## Known Issues:

* Tags can't be deleted (#1)
* When sandboxing `ffmpeg` using `firejail` the default configuration uses a private `/dev` and `/tmp`. I recommend whitelisting `$HOME/.cache` in `/etc/firejail/ffmpeg.local` (e.g `whitelist ${HOME}/.cache`) and setting `TMPDIR` to `$HOME/.cache`

## To Do:

* Re-name the project to something better than its current generic name
* Test, test, test and then test again
* Consider alternative backends such as AtomicParsley (for editing m4a files) and any other software that might exist
* Support embedding album art
* ~~Pre-fill some data with examples, ignore them if the example is un-edited~~
* Try to intelligently auto-fill tag data (integrate [acoustid](https://acoustid.org/))
